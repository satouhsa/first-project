package com.ouhsa.empolyee;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;

public class StaffDemo   {


//    static Scanner obj=new Scanner(System.in);



   static public Volunteer vol=new Volunteer();
   static public SalaryEmployee sa=new SalaryEmployee();
   static public HourlyEmployee h=new HourlyEmployee();

    static Scanner obj=new Scanner(System.in);
   static public void edit(ArrayList<StaffMember>st){
       int b=0;
       int index=0;
        System.out.println("plz input ID you to update:");
        int search=obj.nextInt();

      for(StaffMember tt:st){
          if(tt instanceof Volunteer){
              if(search==tt.getId()){
                  System.out.println("id:"+tt.getId());
                  System.out.println("name:"+tt.getName());
                  System.out.println("address:"+tt.getAddress());
                  System.out.println("=======plz input for update============");
                  System.out.println("input ID:");
                  int id=obj.nextInt();
                  System.out.println("input name:");
                  String name=obj.next();
                  System.out.println("input address:");
                  String address=obj.next();
                  st.set(index,new Volunteer(id,name,address));
                  System.out.println("update successfully");
                  b=1;

              }
              index++;

          }else if(tt instanceof HourlyEmployee){
              if(search==tt.getId()){
                  System.out.println("id:"+tt.getId());
                  System.out.println("name:"+tt.getName());
                  System.out.println("address:"+tt.getAddress());
                  System.out.println("=======plz input for update============");
                  System.out.println("input ID:");
                  int id=obj.nextInt();
                  System.out.println("input name:");
                  String name=obj.next();
                  System.out.println("input address:");
                  String address=obj.next();
                  System.out.println("input hourly:");
                  double hourly=obj.nextDouble();
                  System.out.println("input rate:");
                  double rate=obj.nextDouble();
                  st.set(index,new HourlyEmployee(id,name,address,hourly,rate));
                  System.out.println("update successfully");
                  b=1;
              }
              index++;
          }else if(tt instanceof SalaryEmployee){
              if(search==tt.getId()){
                  System.out.println("=======plz input for update============");
                  System.out.println("input ID:");
                  int id=obj.nextInt();
                  System.out.println("input name:");
                  String name=obj.next();
                  System.out.println("input address:");
                  String address=obj.next();
                  System.out.println("input salary:");
                  double salary=obj.nextDouble();
                  System.out.println("input bonus:");
                  double bonus=obj.nextDouble();
                  st.set(index,new SalaryEmployee(id,name,address,salary,bonus));
                  System.out.println("update successfully");
                  b=1;
              }
              index++;
          }
      }
      if(b==0){
          System.out.println("update not successfully");
      }

    }

    static public void remove(ArrayList<StaffMember>st){

       int b=0;

        System.out.println("Enter ID to remove:");
        int idremove=obj.nextInt();
        for(int i=0;i<st.size();i++){
            if(idremove==st.get(i).getId()){
                st.remove(i);

                System.out.println("Remove successfully");
                b=1;
            }

        }
        if(b==0){
            System.out.println("Remove not successfully");
        }
    }

    public static void main(String[] args) throws IOException {

        Volunteer vol=new Volunteer();
        Scanner obj=new Scanner(System.in);
        ArrayList<StaffMember> st=new ArrayList<StaffMember>();
        st.add(new Volunteer(1,"Ouhsa","Pursat"));
        st.add(new HourlyEmployee(2,"Amanda","Paris",12,44));
        st.add(new SalaryEmployee(3,"jonh","London",2000,100));
        Collections.sort(st);
        for(StaffMember sm:st){
            System.out.println(sm.toString());
        }


        int op = 0;int option = 0;
        do{
            System.out.println("========================================================================\n");
            System.out.println("1/.Add Employee");
            System.out.println("2/.Edit");
            System.out.println("3/.Remove");
            System.out.println("4/.Exit");
            System.out.print("=========================================================================\n");
            System.out.print("Choose Option (1-4):");
            option=obj.nextInt();
            switch (option){
                case 1:{

                    System.out.print("---- ------------------------------------------------------------------------\n");
                    System.out.println("1/. Volunteer");
                    System.out.println("2/. Hourly Emp");
                    System.out.println("3/. Salaries Emp");
                    System.out.println("4/. Back\n");
                    System.out.print("------------------------------------------------------------------------------\n");
                    System.out.print("Choose Option (1-4):");
                    op=obj.nextInt();
                    switch (op){
                        case 1:{
                            System.out.println("==================INSERT VOLUNTEER================");
                            System.out.print("Input ID:");
                            int id=obj.nextInt();
                            System.out.print("Input Name:");
                            String name=obj.next();
                            System.out.print("Input Address:");
                            String address=obj.next();
                            Volunteer vol1=new Volunteer(id,name,address);
                            st.add(vol1);
                            Collections.sort(st);
                            for(StaffMember vv:st){
                                System.out.println(vv.toString());
                            }
                            System.in.read();
                        }break;
                        case 2:{
                            System.out.println("===================INSERT HOURLY=====================");
                            System.out.print("Input ID:");
                            int id=obj.nextInt();
                            System.out.print("Input Name:");
                            String name=obj.next();
                            System.out.print("Input Address:");
                            String address=obj.next();
                            System.out.print("Input Hour Worked:");
                            double hourWorked=obj.nextDouble();
                            System.out.print("Input Rate:");
                            double rate=obj.nextDouble();
                            HourlyEmployee hm=new HourlyEmployee(id,name,address,hourWorked,rate);
                            st.add(hm);
                            Collections.sort(st);
                            for(StaffMember vv:st){
                                System.out.println(vv.toString());
                            }

                            System.in.read();
                        }break;
                        case 3:{
                            System.out.println("=================INSERT SALARY=========================");
                            System.out.print("Input ID:");
                            int id=obj.nextInt();
                            System.out.print("Input Name:");
                            String name=obj.next();
                            System.out.print("Input Address:");
                            String address=obj.next();
                            System.out.print("Input Salary:");
                            double salary=obj.nextDouble();
                            System.out.print("Input Bonus:");
                            double bonus=obj.nextDouble();
                            SalaryEmployee s=new SalaryEmployee(id,name,address,salary,bonus);
                            st.add(s);
                            Collections.sort(st);
                            for(StaffMember vv:st){
                                System.out.println(vv.toString());
                            }
                            System.in.read();
                        }break;
                        case 4:{
                            System.in.read();
                        }break;
                    }

                }break;

                case 2:{
                    System.out.println("==================Edit================");

                            StaffDemo de =new StaffDemo();
                            de.edit(st);
                            for (StaffMember sm:st){
                                System.out.println(sm.toString());
                            }
                            break;



                }
                case 3:{
                    System.out.println("=================Remove===============");

                    StaffDemo de =new StaffDemo();
                    de.remove(st);
                    for (StaffMember sm:st){
                        System.out.println(sm.toString());
                    }


                    break;

                }
                case 4:{
                    System.out.println("===========Exit==========");
                    System.out.println("good bye");
                    System.exit(0);
                }
            }


        }while(option!=4);
    }


}

