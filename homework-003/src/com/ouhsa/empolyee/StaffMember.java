package com.ouhsa.empolyee;

public abstract class StaffMember implements Comparable<StaffMember>{


    protected int id;
    protected String name;
    protected String address;
    public StaffMember(){}
    public StaffMember(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString()
    {
        return "ID:"+id+"\nName:"+name+"\nAddress:"+address;
    }
    double pay(){
        return 0;
    }
    void dispay(){
        System.out.print("ID:"+id+"\nName:"+name+"\nAddress:"+address);
    }
    public int compareTo(StaffMember sm) {
        if(getName().compareTo(sm.getName())>0) return 1;
        if(getName().compareTo(sm.getName())<0) return -1;
        return 0;
    }
}
