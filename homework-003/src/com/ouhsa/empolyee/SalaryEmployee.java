package com.ouhsa.empolyee;

import java.util.ArrayList;
import java.util.Scanner;

public class SalaryEmployee extends StaffMember {

    private double salary;
    private double bonus;
    ArrayList<SalaryEmployee> sep=new ArrayList<>();
    Scanner obj=new Scanner(System.in);
    public SalaryEmployee(int id, String name, String address ,double salary,double bonus) {
        super(id, name, address);
        this.salary=salary;
        this.bonus=bonus;
    }
    public SalaryEmployee(){

    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        System.out.println("---------------------------");
        return super.toString()+"\nSalary:"+salary+"\nBonus:"+bonus+"\nPay:"+(this.pay()==-1?null:this.pay());
    }
    public double pay(){
        if(salary<0||bonus<0){
            return -1;
     }else{
            return salary+bonus;
    }

    }
    void addSemp() {
        System.out.print("Input ID:");
        id=obj.nextInt();
        System.out.print("Input Name:");
        name=obj.next();
        System.out.print("Input Address:");
        address=obj.next();
        System.out.print("Input Salary:");
        salary=obj.nextDouble();
        System.out.print("Input Bonus:");
        bonus=obj.nextDouble();
        SalaryEmployee s=new SalaryEmployee(id,name,address,salary,bonus);
        sep.add(s);
    }

   public void display() {
        for(SalaryEmployee sal:sep){
            System.out.println(sal);
        }
    }

}
