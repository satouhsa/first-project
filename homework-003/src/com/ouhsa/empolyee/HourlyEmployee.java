package com.ouhsa.empolyee;

import java.util.ArrayList;
import java.util.Scanner;

public class HourlyEmployee extends StaffMember {






    private double hourWorked;
    private double rate;
    Scanner obj=new Scanner(System.in);
    ArrayList<HourlyEmployee> h=new ArrayList<HourlyEmployee>();
    public HourlyEmployee(int id, String name, String address,double hourWorked,double rate) {
        super(id, name, address);
        this.hourWorked=hourWorked;
        this.rate=rate;
    }
    public HourlyEmployee(){

    }

    @Override
    public String toString() {
        System.out.println("---------------------------");
        return super.toString()+"\nHourWorked:"+hourWorked+"\nRate:"+rate+"\nPay:"+pay();
    }
    public double pay(){
        return hourWorked*rate;
    }
    public void addHourEmp(){
        System.out.print("Input ID:");
        id=obj.nextInt();
        System.out.print("Input Name:");
        name=obj.next();
        System.out.print("Input Address:");
        address=obj.next();
        System.out.print("Input Hour Worked:");
        hourWorked=obj.nextDouble();
        System.out.print("Input Rate:");
        rate=obj.nextDouble();
        HourlyEmployee hm=new HourlyEmployee(id,name,address,hourWorked,rate);
        h.add(hm);
    }
    public void dispay(){
        for(HourlyEmployee  he:h){
            System.out.println(he);
        }
    }

}
